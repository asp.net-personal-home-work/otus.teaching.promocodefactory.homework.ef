﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPreferenceRepository : IRepository<Preference>
    {
        Task<IReadOnlyCollection<Preference>> GetPreferencesByIdCollectionAsync(IEnumerable<Guid> idColl);

        Task<Preference> GetPreferenceByNameAsync(string name);
    }
}
