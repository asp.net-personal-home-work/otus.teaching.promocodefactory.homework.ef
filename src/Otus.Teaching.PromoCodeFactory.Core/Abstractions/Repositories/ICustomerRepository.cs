﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository: IRepository<Customer>
    {
        Task<Customer> GetByIdWithPromoAndPreferencesAsync(Guid id);

        Task<IReadOnlyCollection<Customer>> GetCustomersByFilter(CustomerFilter customerFilter);
    }

    public class CustomerFilter
    {
        public string Preference { get; set; }
    }
}
