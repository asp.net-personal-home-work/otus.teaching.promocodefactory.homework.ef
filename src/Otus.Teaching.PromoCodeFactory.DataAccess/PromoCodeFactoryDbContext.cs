﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfiguration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromoCodeFactoryDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }


        public PromoCodeFactoryDbContext(DbContextOptions<PromoCodeFactoryDbContext> options) : base(options)
        {
        }

        public void SeedDataBase()
        {
            Roles.AddRange(FakeDataFactory.Roles.ToList());

            foreach (var employee in FakeDataFactory.Employees)
            {
                employee.Role = Roles.Local.FirstOrDefault(r => r.Id == employee.Role.Id);
                Employees.Add(employee);
            }
            
            Preferences.AddRange(FakeDataFactory.Preferences);

            foreach (var customer in FakeDataFactory.Customers)
            {
                var tempPrefList = new List<Preference>();
                foreach (var preference in customer.Preferences)
                {
                    tempPrefList.Add(Preferences.Local.FirstOrDefault(p => p.Id.Equals(preference.Id)));
                }
                customer.Preferences = tempPrefList;
                Customers.Add(customer);
            }
            
            SaveChanges();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new PromoCodeCodeConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
    }
}
