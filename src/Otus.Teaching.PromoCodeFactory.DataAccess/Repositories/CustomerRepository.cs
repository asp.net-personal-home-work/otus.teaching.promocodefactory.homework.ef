﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(PromoCodeFactoryDbContext dbContext) : base(dbContext)
        {
        }

        public Task<Customer> GetByIdWithPromoAndPreferencesAsync(Guid id)
        {
            return DbContext.Set<Customer>()
                .AsQueryable()
                .Include(c => c.Preferences)
                .Include(c => c.PromoCodes)
                .FirstOrDefaultAsync(c => c.Id.Equals(id));
        }

        public async Task<IReadOnlyCollection<Customer>> GetCustomersByFilter(CustomerFilter customerFilter)
        {
            return await DbContext.Set<Customer>()
                .Include(c => c.Preferences)
                .Include(c => c.PromoCodes)
                .Where(c => c.Preferences.Any(p => p.Name.Equals(customerFilter.Preference)))
                .ToListAsync();
        }
    }
}
