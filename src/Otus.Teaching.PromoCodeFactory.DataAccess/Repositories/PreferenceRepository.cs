﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository(PromoCodeFactoryDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IReadOnlyCollection<Preference>> GetPreferencesByIdCollectionAsync(IEnumerable<Guid> idColl)
        {
            return await DbContext.Set<Preference>()
                            .Where(p => idColl.Contains(p.Id))
                            .ToListAsync();
        }

        public async Task<Preference> GetPreferenceByNameAsync(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            return await DbContext.Set<Preference>()
                            .Where(p => p.Name.Equals(name))
                            .FirstOrDefaultAsync();
        }
    }
}
