﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfiguration
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");

            builder.HasKey(x => x.Id);

            builder.Property(q => q.FirstName)
                .HasMaxLength(64);

            builder.Property(q => q.LastName)
                .HasMaxLength(64);

            builder.Ignore(q => q.FullName);

            builder.Property(q => q.Email)
                .HasMaxLength(64);

            builder.HasMany(q => q.Preferences)
                .WithMany(q => q.Customers)
                .UsingEntity(q => q.ToTable("CustomerPreference"));

            builder.HasMany(q => q.PromoCodes)
                .WithOne()
                .HasForeignKey("CustomerId")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
