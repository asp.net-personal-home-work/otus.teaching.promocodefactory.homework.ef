﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfiguration
{
    internal class PreferenceConfiguration : IEntityTypeConfiguration<Preference>
    {
        public void Configure(EntityTypeBuilder<Preference> builder)
        {
            builder.ToTable("Preferences");
            
            builder.HasKey(q => q.Id);
            
            builder.Property(q => q.Name)
                .HasMaxLength(64);
         
            builder.HasMany<PromoCode>()
                .WithOne(q => q.Preference)
                .HasForeignKey("PreferenceId");
        }
    }
}
