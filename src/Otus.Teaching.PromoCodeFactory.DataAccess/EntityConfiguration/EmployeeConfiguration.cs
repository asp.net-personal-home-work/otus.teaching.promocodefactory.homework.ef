﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfiguration
{
    internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees");
           
            builder.HasKey(q => q.Id);
            
            builder.Property(q => q.FirstName)
                .HasMaxLength(64);
            
            builder.Property(q => q.LastName)
                .HasMaxLength(64);
            
            builder.Ignore(q => q.FullName);
            
            builder.Property(q => q.Email)
                .HasMaxLength(64);
           
            builder.HasOne(q => q.Role)
                .WithOne()
                .HasForeignKey<Employee>("RoleId");
        }
    }

}
