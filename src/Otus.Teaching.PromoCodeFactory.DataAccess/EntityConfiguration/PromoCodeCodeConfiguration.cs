﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfiguration
{
    internal class PromoCodeCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.ToTable("PromoCodes");

            builder.HasKey(q => q.Id);

            builder.Property(q => q.Code)
                .HasMaxLength(32);

            builder.Property(q => q.PartnerName)
                .HasMaxLength(256);

            builder.Property(q => q.ServiceInfo)
                .HasMaxLength(512);

            builder.HasOne(q => q.PartnerManager)
                .WithMany()
                .HasForeignKey("PartnerManagerId");
        }
    }
}
