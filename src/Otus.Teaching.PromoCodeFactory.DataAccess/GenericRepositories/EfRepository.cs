﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbSet<T> _dbSet;
        
        protected readonly DbContext DbContext;

        public EfRepository(PromoCodeFactoryDbContext dbContext)
        {
            DbContext = dbContext;
            _dbSet = DbContext.Set<T>();
        }

        public async Task<T> CreateAsync(T item)
        {
            if (item == null)
            {
                throw new ArgumentException(nameof(item));
            }

            var createdItem = _dbSet.Add(item);

            await DbContext.SaveChangesAsync();
         
            return createdItem.Entity;
        }

        public async Task DeleteAsync(T item)
        {
            if (item == null)
            {
                throw new ArgumentException(nameof(item));
            }

            _dbSet.Remove(item);

            await DbContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<T>> GetAllAsync()
        {
            return (await _dbSet.ToListAsync()).AsReadOnly();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<T> UpdateAsync(T item)
        {
            if (item == null)
            {
                throw new ArgumentException(nameof(item));
            }

            var itemEf = _dbSet.Update(item);

            await DbContext.SaveChangesAsync();

            return itemEf.Entity;
        }
    }
}
