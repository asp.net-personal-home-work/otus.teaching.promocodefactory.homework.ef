﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.GenericRepositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }

        public Task<IReadOnlyCollection<T>> GetAllAsync()
        {
            return Task.FromResult((IReadOnlyCollection<T>)Data.AsReadOnly());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T item)
        {
            item.Id = Guid.NewGuid();
            Data.Add(item);
            return Task.FromResult(item);
        }

        public Task<T> UpdateAsync(T item)
        {
            T existedItem = Data.FirstOrDefault(i => i.Id.Equals(item.Id));

            if (existedItem is null)
            {
                throw new InvalidOperationException($"Попытка обновить не существующий объект id = {item.Id}");
            }

            Data.Remove(existedItem);

            Data.Add(item);

            return Task.FromResult(item);
        }

        public Task DeleteAsync(T item)
        {
            T existedItem = Data.FirstOrDefault(i => i.Id.Equals(item.Id));

            if (existedItem is null) return Task.CompletedTask;

            Data.Remove(existedItem);

            return Task.CompletedTask;
        }
    }
}