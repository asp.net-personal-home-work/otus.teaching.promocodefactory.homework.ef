﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly ICustomerRepository _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository,
            IPreferenceRepository preferenceRepository,
            ICustomerRepository customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();

            return Ok(promocodes.Select(x => new PromoCodeShortResponse
                    {
                        Id = x.Id,
                        Code = x.Code,
                        ServiceInfo = x.ServiceInfo,
                        BeginDate = x.BeginDate.ToString(),
                        EndDate = x.EndDate.ToString(),
                        PartnerName = x.PartnerName
                    }
            ));
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            Preference preference = await _preferenceRepository.GetPreferenceByNameAsync(request.Preference);
          
            if (preference is null)
            {
                return NotFound("There is no such preference");
            }

            var promoCode = new PromoCode
            {
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(14),
                Preference = preference
            };

            var createdPromo = _promoCodeRepository.CreateAsync(promoCode);
            
            if (createdPromo is null)
            {
                return BadRequest();
            }

            IReadOnlyCollection<Customer> customers = await _customerRepository.GetCustomersByFilter(new CustomerFilter { Preference = request.Preference });

            if (!customers.Any())
            {
                return NotFound("customers not found");
            }

            foreach (var customer in customers)
            {
                customer.PromoCodes.Add(promoCode);

                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}