﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository _preferenceRepository;

        public CustomersController(ICustomerRepository customerRepository, IPreferenceRepository preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<ICollection<CustomerShortResponse>>> GetCustomersAsync()
        {
            IEnumerable<Customer> customerList = await _customerRepository.GetAllAsync();

            return Ok(customerList.Select(c => new CustomerShortResponse
                {
                    Id = c.Id,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Email = c.Email,
                }
            ));
        }

        /// <summary>
        /// Получить клиента с выданными ему промокодами и предпочтениями по Id
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid? id)
        {
            if (id is null)
            {
                return BadRequest();
            }

            Customer customer = await _customerRepository.GetByIdWithPromoAndPreferencesAsync(id.Value);

            if (customer is null)
            {
                return NotFound();
            }

            return Ok(new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes?.Select(q => new PromoCodeShortResponse
                {
                    Id = q.Id,
                    Code = q.Code,
                    PartnerName = q.PartnerName,
                    ServiceInfo = q.ServiceInfo,
                    BeginDate = q.BeginDate.ToString("d"),
                    EndDate = q.EndDate.ToString("d")
                }).ToList(),
                Preferences = customer.Preferences?.Select(p => new PreferenceResponse
                {
                    Id = p.Id,
                    Name = p.Name,
                }).ToList()
            });
        }

        /// <summary>
        /// Создать пользователя с предпочтениями
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            IEnumerable<Preference> preferences = await _preferenceRepository.GetPreferencesByIdCollectionAsync(request.PreferenceIds);

            var newCustomer = await _customerRepository.CreateAsync(new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.ToList()
            });
         
            return Ok(newCustomer.Id);
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями и промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdWithPromoAndPreferencesAsync(id);

            if (customer is null)
            {
                return NotFound();
            }
            
            IEnumerable<Preference> preferences = await _preferenceRepository.GetPreferencesByIdCollectionAsync(request.PreferenceIds);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.ToList();
            customer.PromoCodes = customer.PromoCodes;

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }
        
        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null)
            {
                return NotFound();
            }
            
            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}